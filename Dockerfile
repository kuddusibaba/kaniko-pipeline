FROM ubuntu:latest

RUN apt update -y && apt upgrade -y

RUN apt-get install nginx -y

RUN apt-get install python3 -y
